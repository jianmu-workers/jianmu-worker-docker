// Copyright (c) 2022 by Duguang.IO Inc. All Rights Reserved.
// Author: Ethan Liu
// Date: 2022-05-20 18:40:51

package command

import (
	"os"
	"runtime"
	"strconv"

	"github.com/kelseyhightower/envconfig"
	"github.com/shirou/gopsutil/v3/mem"
)

// Config 系统配置
type Config struct {
	Debug bool `envconfig:"JIANMU_DEBUG"`
	Trace bool `envconfig:"JIANMU_TRACE"`

	Logger struct {
		File       string `envconfig:"JIANMU_LOG_FILE"`
		MaxAge     int    `envconfig:"JIANMU_LOG_FILE_MAX_AGE"     default:"1"`
		MaxBackups int    `envconfig:"JIANMU_LOG_FILE_MAX_BACKUPS" default:"1"`
		MaxSize    int    `envconfig:"JIANMU_LOG_FILE_MAX_SIZE"    default:"100"`
	}

	Server struct {
		Port string `envconfig:"JIANMU_HTTP_BIND" default:":3000"`
	}

	Client struct {
		Address string `envconfig:"JIANMU_SRV_ADDRESS"   required:"true"`
		Secret  string `envconfig:"JIANMU_SRV_SECRET"    required:"true"`
	}

	Platform struct {
		OS   string `envconfig:"JIANMU_PLATFORM_OS"`
		Arch string `envconfig:"JIANMU_PLATFORM_ARCH"`
	}

	Node struct {
		Enable    bool   `envconfig:"JIANMU_NODE_ENABLE"  default:"false"`
		Bind      string `envconfig:"JIANMU_NODE_BIND" default:"0.0.0.0:7777"`
		Advertise string `envconfig:"JIANMU_NODE_ADVERTISE"`
		Rpc_addr  string `envconfig:"JIANMU_RPC_ADDR" default:"127.0.0.1:7373"`
		Join      string `envconfig:"JIANMU_JOIN_ADDRS"`
	}

	Worker struct {
		ID              string  `envconfig:"JIANMU_WORKER_ID"    required:"true"`
		Name            string  `envconfig:"JIANMU_WORKER_NAME"`
		Capacity        int     `envconfig:"JIANMU_WORKER_CAPACITY" default:"2"`
		Procs           int64   `envconfig:"JIANMU_WORKER_MAX_PROCS"`
		Tags            string  `envconfig:"JIANMU_WORKER_TAGS"`
		Type            string  `envconfig:"JIANMU_WORKER_TYPE" default:"DOCKER"`
		EnvFile         string  `envconfig:"JIANMU_WORKER_ENVFILE"`
		Path            string  `envconfig:"JIANMU_WORKER_PATH"`
		Root            string  `envconfig:"JIANMU_WORKER_ROOT"`
		Cpus            float64 `envconfig:"JIANMU_WORKER_CPUS"`
		Memory          string  `envconfig:"JIANMU_WORKER_MEMORY"`
		RegisterTimeout int     `envconfig:"JIANMU_WORKER_REGISTER_TIMEOUT" default:"0"`
	}

	//
	// Worker启动容器的配置
	//
	ContainerConfig struct {
		// 使用多少个CPU,最少0.01个CPU,最大本机所有的核心数,0表示无限制.
		Cpus float64 `envconfig:"JIANMU_WORKER_CONTAINER_CPUS"`
		// Memory limit (format: <number>[<unit>]). Number is a positive integer. Unit can be one of b, k, m, or g. Minimum is 4M. zero means no memory limit.
		Memory string `envconfig:"JIANMU_WORKER_CONTAINER_MEMORY"`
		// 网络模型 bridge host none default or自定义
		NetworkMode string `envconfig:"JIANMU_WORKER_CONTAINER_NETWORK_MODE" default:"default"`
		// 容器运行最大超时时间，单位为秒，未配置时默认值为18000秒
		Timeout int `envconfig:"JIANMU_WORKER_CONTAINER_TIMEOUT" default:"18000"`
	}
}

// FromEnviron 从环境变量载入配置
func FromEnviron() (Config, error) {
	var config Config
	err := envconfig.Process("", &config)
	if err != nil {
		return config, err
	}
	if config.Worker.Name == "" {
		config.Worker.Name, _ = os.Hostname()
	}
	if config.Platform.OS == "" {
		config.Platform.OS = runtime.GOOS
	}
	if config.Platform.Arch == "" {
		config.Platform.Arch = runtime.GOARCH
	}
	if config.ContainerConfig.Cpus == 0 {
		config.ContainerConfig.Cpus = float64(runtime.NumCPU())
	}
	if config.Worker.Cpus == 0 {
		config.Worker.Cpus = float64(runtime.NumCPU())
	}
	if config.Worker.Memory == "" {
		vmStat, _ := mem.VirtualMemory()
		config.Worker.Memory = strconv.FormatUint(vmStat.Total, 10)
	}

	return config, nil
}
