// Copyright (c) 2023 by Duguang.IO Inc. All Rights Reserved.
// Author: Ethan Liu
// Date: 2023-02-01 20:42:13

package command

import (
	"os"

	"github.com/hashicorp/serf/cmd/serf/command"
	c "github.com/mitchellh/cli"
)

type JoinCmd struct {
	Addr string `arg:"" name:"addr" help:"join the cluster."`
}

func (j *JoinCmd) Run() error {
	ui := &c.BasicUi{Writer: os.Stdout}
	cmd := &command.JoinCommand{
		Ui: ui,
	}
	cmd.Run([]string{j.Addr})
	return nil
}
