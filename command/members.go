// Copyright (c) 2023 by Duguang.IO Inc. All Rights Reserved.
// Author: Ethan Liu
// Date: 2023-02-01 20:34:43

package command

import (
	"os"

	"github.com/hashicorp/serf/cmd/serf/command"
	c "github.com/mitchellh/cli"
)

type MembersCmd struct{}

func (m *MembersCmd) Run() error {
	ui := &c.BasicUi{Writer: os.Stdout}
	cmd := &command.MembersCommand{
		Ui: ui,
	}
	cmd.Run([]string{})
	return nil
}
