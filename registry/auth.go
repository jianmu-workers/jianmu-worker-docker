// Copyright (c) 2022 by Duguang.IO Inc. All Rights Reserved.
// Author: Ethan Liu
// Date: 2022-05-04 11:34:41

package registry

import (
	"encoding/base64"
	"encoding/json"
)

type (
	// config Docker客户端配置
	config struct {
		Auths map[string]auth `json:"auths"`
	}

	// auth 保存registry的认证字符串
	auth struct {
		Auth     string `json:"auth"`
		Username string `json:"username,omitempty"`
		Password string `json:"password,omitempty"`
	}
)

// Header 返回docker registry的认证头，使用base64编码后的json串
func Header(username, password string) string {
	v := struct {
		Username string `json:"username,omitempty"`
		Password string `json:"password,omitempty"`
	}{
		Username: username,
		Password: password,
	}
	buf, _ := json.Marshal(&v)
	return base64.URLEncoding.EncodeToString(buf)
}
