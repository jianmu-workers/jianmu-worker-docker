// Copyright (c) 2022 by Duguang.IO Inc. All Rights Reserved.
// Author: Ethan Liu
// Date: 2022-05-04 18:00:25

package engine

type (
	// Runner 执行器定义
	Runner struct {
		ID         string     `json:"taskInstanceId,omitempty"`
		Version    int        `json:"version"`
		Type       string     `json:"type,omitempty"`
		Auth       *Auth      `json:"auth,omitempty"`
		Pull       PullPolicy `json:"pullStrategy,omitempty"`
		Spec       Spec       `json:"containerSpec,omitempty"`
		ResultFile string     `json:"resultFile,omitempty"`
		Volume     Volume     `json:"volume,omitempty"`
		Timeout    int        `json:"timeout,omitempty"`
	}

	// Spec 容器定义
	Spec struct {
		Image        string            `json:"image,omitempty"`
		Network      string            `json:"network,omitempty"`
		Networks     []string          `json:"networks,omitempty"`
		Labels       map[string]string `json:"labels,omitempty"`
		WorkingDir   string            `json:"working_dir,omitempty"`
		User         string            `json:"user,omitempty"`
		Host         string            `json:"host,omitempty"`
		Sock         string            `json:"sock,omitempty"`
		Envs         map[string]string `json:"environment,omitempty"`
		Secrets      []*Secret         `json:"secrets,omitempty"`
		Entrypoint   []string          `json:"entrypoint,omitempty"`
		Command      []string          `json:"args,omitempty"`
		Volumes      []*VolumeMount    `json:"volume_mounts,omitempty"`
		Privileged   bool              `json:"privileged,omitempty"`
		ShmSize      int64             `json:"shm_size,omitempty"`
		DNS          []string          `json:"dns,omitempty"`
		DNSSearch    []string          `json:"dns_search,omitempty"`
		ExtraHosts   []string          `json:"extra_hosts,omitempty"`
		CPUPeriod    int64             `json:"cpu_period,omitempty"`
		CPUQuota     int64             `json:"cpu_quota,omitempty"`
		CPUShares    int64             `json:"cpu_shares,omitempty"`
		CPUSet       []string          `json:"cpu_set,omitempty"`
		CPUS         float64           `json:"cpus,omitempty"`
		NetworkMode  string            `json:"network_mode,omitempty"`
		MemSwapLimit int64             `json:"memswap_limit,omitempty"`
		MemLimit     int64             `json:"mem_limit,omitempty"`
		Devices      []*VolumeDevice   `json:"devices,omitempty"`
		Detach       bool              `json:"detach,omitempty"`
		Platform     Platform          `json:"platform,omitempty"`
	}

	// VolumeDevice 容器要挂载的Volume设备
	VolumeDevice struct {
		Name       string `json:"name,omitempty"`
		DevicePath string `json:"path,omitempty"`
	}

	// VolumeMount 容器要挂载的卷
	VolumeMount struct {
		Source string `json:"source,omitempty"`
		Target string `json:"target,omitempty"`
	}

	// Network 容器使用的网络
	Network struct {
		ID      string            `json:"id,omitempty"`
		Labels  map[string]string `json:"labels,omitempty"`
		Options map[string]string `json:"options,omitempty"`
	}

	// Secret 密钥
	Secret struct {
		Env  string `json:"env,omitempty"`
		Data []byte `json:"data,omitempty"`
		Mask bool   `json:"mask,omitempty"`
	}

	// Auth 镜像拉取认证信息
	Auth struct {
		Address  string `json:"address,omitempty"`
		Username string `json:"username,omitempty"`
		Password string `json:"password,omitempty"`
	}

	// Platform defines the target platform.
	Platform struct {
		OS      string `json:"os,omitempty"`
		Arch    string `json:"arch,omitempty"`
		Variant string `json:"variant,omitempty"`
		Version string `json:"version,omitempty"`
	}

	Volume struct {
		Name string `json:"name,omitempty"`
		Type string `json:"type,omitempty"`
	}

	// Line 容器日志行
	Line struct {
		Number    int    `json:"number"`
		Message   string `json:"content"`
		Timestamp int64  `json:"timestamp"`
	}

	Task struct {
		TaskId     string     `json:"taskId,omitempty"`
		Status     TaskStatus `json:"status"`
		ExitCode   int        `json:"exitCode"`
		ResultFile string     `json:"resultFile,omitempty"`
		ErrorMsg   string     `json:"errorMsg,omitempty"`
	}

	Worker struct {
		// Worker ID
		ID string
		// Worker Name
		Name string `json:"name,omitempty"`
		// Worker Type
		Type string `json:"type,omitempty"`
		// worker tags
		Tag string `json:"tag,omitempty"`
		// worker OS
		Os string `json:"os,omitempty"`
		// worker Arch
		Arch string `json:"arch,omitempty"`
		// worker Capacity
		Capacity int `json:"capacity,omitempty"`
	}

	// State 容器执行情况
	State struct {
		// ExitCode 退出代码
		ExitCode int

		// GetExited 是否已退出
		Exited bool

		// OOMKilled 是否被OOMKill终止
		OOMKilled bool

		// ResultFile 结果文件
		ResultFile string
	}
)

func (s *Secret) GetValue() string { return string(s.Data) }
func (s *Secret) IsMasked() bool   { return s.Mask }
