// Copyright (c) 2022 by Duguang.IO Inc. All Rights Reserved.
// Author: Ethan Liu
// Date: 2022-05-03 23:29:52

package main

import (
	"jianmu-worker-docker/command"
)

func main() {
	command.Command()
}
