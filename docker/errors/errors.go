// Copyright (c) 2022 by Duguang.IO Inc. All Rights Reserved.
// Author: Ethan Liu
// Date: 2022-05-05 13:50:18

package errors

import (
	"errors"
	"strings"
)

// TrimExtraInfo 去除docker返回的错误信息中的额外信息，有可能会暴露敏感信息
func TrimExtraInfo(err error) error {
	if err == nil {
		return nil
	}
	s := err.Error()
	i := strings.Index(s, "extra info:")
	if i > 0 {
		s = s[:i]
		s = strings.TrimSpace(s)
		s = strings.TrimSuffix(s, "(0x2)")
		s = strings.TrimSpace(s)
		return errors.New(s)
	}
	return err
}
