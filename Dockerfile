FROM docker.jianmuhub.com/library/alpine:3.17.0
ARG TARGETOS
ARG TARGETARCH
ADD ./${TARGETOS}/${TARGETARCH}/jianmu-worker-docker /jianmu-worker-docker
ADD version /version
ENTRYPOINT ["/jianmu-worker-docker"]
CMD ["daemon"]