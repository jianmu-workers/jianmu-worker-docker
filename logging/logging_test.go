// Copyright (c) 2022 by Duguang.IO Inc. All Rights Reserved.
// Author: Ethan Liu
// Date: 2022-05-04 23:19:31
// LastEditors: ethan-liu ethan-liu@outlook.com

package logging

import (
	"context"
	"testing"

	"github.com/google/go-cmp/cmp"
	"github.com/sirupsen/logrus"
	"gotest.tools/assert"
)

type formatterStub struct {
	entries []*logrus.Entry
}

func (f *formatterStub) Format(entry *logrus.Entry) ([]byte, error) {
	f.entries = append(f.entries, entry)
	return nil, nil
}

func TestFromContext(t *testing.T) {
	ctx := context.Background()

	t.Run("returns a default value before init", func(t *testing.T) {
		formatter := &formatterStub{}
		msg := "no default fields yet"
		logger := FromContext(ctx)
		setFormatter(t, logger, formatter)
		logger.Warn(msg)
		expected := []*logrus.Entry{
			{
				Level:   logrus.WarnLevel,
				Message: msg,
				Data:    logrus.Fields{},
			},
		}
		assert.DeepEqual(t, formatter.entries, expected, cmpEntry)
	})

	t.Run("returns a logger with fields", func(t *testing.T) {
		formatter := &formatterStub{}
		msg := "with fields"
		ctx := WithField(ctx, "key", 12345)
		ctx = WithFields(ctx, logrus.Fields{
			"another-key": "ok",
		})
		logger := FromContext(ctx)
		setFormatter(t, logger, formatter)
		logger.Info(msg)

		expected := []*logrus.Entry{
			{
				Level:   logrus.InfoLevel,
				Message: msg,
				Data: logrus.Fields{
					"key":         12345,
					"another-key": "ok",
				},
			},
		}
		assert.DeepEqual(t, formatter.entries, expected, cmpEntry)
	})
}

var cmpEntry = cmp.Comparer(func(x, y logrus.Entry) bool {
	return x.Message == y.Message && x.Level == y.Level && cmp.Equal(x.Data, y.Data)
})

func setFormatter(t *testing.T, logger logrus.FieldLogger, formatter logrus.Formatter) {
	switch logger := logger.(type) {
	case *logrus.Entry:
		logger.Logger.Formatter = formatter
	default:
		t.Fatalf("unexpected logger type %T", logger)
	}
}
