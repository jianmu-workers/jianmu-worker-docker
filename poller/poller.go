// Copyright (c) 2022 by Duguang.IO Inc. All Rights Reserved.
// Author: Ethan Liu
// Date: 2022-05-18 11:24:06

package poller

import (
	"context"
	"sync"

	"jianmu-worker-docker/client"
	"jianmu-worker-docker/engine"

	logger "jianmu-worker-docker/logging"
)

var noContext = context.Background()

// Poller 从服务器拉取任务执行
type Poller struct {
	Client client.Client
	Filter *client.Filter

	// 分发任务执行
	Dispatch func(context.Context, *engine.Runner) error
}

// Poll 开启n个连接并发获取任务
func (p *Poller) Poll(ctx context.Context, n int) {
	var wg sync.WaitGroup
	for i := 0; i < n; i++ {
		wg.Add(1)
		go func(i int) {
			for {
				select {
				case <-ctx.Done():
					wg.Done()
					return
				default:
					p.poll(ctx, i+1)
				}
			}
		}(i)
	}

	wg.Wait()
}

// poll 拉取任务并分发执行
func (p *Poller) poll(ctx context.Context, thread int) error {
	log := logger.FromContext(ctx).WithField("thread", thread)
	log.WithField("thread", thread).Debug("poller: request runner from ci server")

	runner, err := p.Client.Request(ctx, p.Filter)
	if err == context.Canceled || err == context.DeadlineExceeded {
		log.WithError(err).Trace("poller: no runner returned")
		return nil
	}
	if err != nil {
		log.WithError(err).Error("poller: cannot request runner")
		return err
	}

	if runner == nil || runner.ID == "" {
		return nil
	}

	return p.Dispatch(
		logger.WithLogger(noContext, log), runner)
}
